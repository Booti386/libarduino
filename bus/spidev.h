#ifndef LIBARDUINO__BUS__SPIDEV_H
#define LIBARDUINO__BUS__SPIDEV_H 1

#include <SPI.h>

struct SPIDev
{
	static constexpr uint32_t getSPIClockHz(void);
	static constexpr uint8_t getSPIEndian(void);
	static constexpr uint8_t getSPIMode(void);

	uint8_t ssPin;

	inline SPIDev(uint8_t ss_pin):
		ssPin(ss_pin) {};
};

#endif /* LIBARDUINO__BUS__SPIDEV_H */
