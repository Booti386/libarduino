#ifndef LIBARDUINO__BASE__INTMGR_H
#define LIBARDUINO__BASE__INTMGR_H 1

#include <avr/interrupt.h>
#include <avr/sleep.h>

namespace IntMgr
{
	/*** Types ***/

	packed_enum ISRStatus
	{
		DONTCARE,
		WAKEUP
	};

	using isr_cb_t = ISRStatus (*)(uint8_t pin, void *arg);

	struct ISRCtx
	{
		int8_t pin;
		isr_cb_t isrCb;
		void *arg;
		struct ISRCtx *next;

		inline ISRCtx(int8_t pin, isr_cb_t isr_cb, void *arg = nullptr):
			pin(pin),
			isrCb(isr_cb),
			arg(arg),
			next(nullptr) {}
	};

	/*** Variables ***/

	static volatile bool wakeup = false;

	static struct ISRCtx *isrCtx = nullptr;

	/*** Functions ***/

	static inline void isr(uint8_t pin)
	{
		struct ISRCtx *p = isrCtx;

		while (p)
		{
			ISRStatus status;

			if (p->pin != pin)
				continue;

			status = p->isrCb(pin, p->arg);
			if (status == WAKEUP)
				wakeup = true;

			p = p->next;
		}
	}

	template <uint8_t pin>
	static inline bool attachPin(uint8_t mode)
	{
		uint8_t num = digitalPinToInterrupt(pin);
		if (num == NOT_AN_INTERRUPT)
			return false;

		pinMode(pin, INPUT_PULLUP);
		attachInterrupt(num, [] { isr(pin); }, mode);
		return true;
	}

	template <uint8_t pin>
	static inline bool detachPin(void)
	{
		uint8_t num = digitalPinToInterrupt(pin);
		if (num == NOT_AN_INTERRUPT)
			return false;

		detachInterrupt(num);
		return true;
	}

	static inline void bindISR(ISRCtx &isr_ctx)
	{
		if (isrCtx)
			isrCtx->next = &isr_ctx;

		isrCtx = &isr_ctx;
	}

	static inline int8_t poll(void)
	{
		while (true)
		{
			set_sleep_mode(SLEEP_MODE_PWR_DOWN);

			cli();
			if (!wakeup)
			{
				sleep_enable();
				sleep_bod_disable();
				sei();
				sleep_cpu();
				sleep_disable();
			}
			sei();
		}

		return 0;
	}
};

#endif /* LIBARDUINO__BASE__INTMGR_H */
