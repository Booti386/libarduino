#ifndef LIBARDUINO__RADIO__NRF24L01P_H
#define LIBARDUINO__RADIO__NRF24L01P_H 1

namespace NRF24L01P
{
	namespace Reg
	{
		packed_enum Type
		{
			CFG = 0x00,
			EN_AA = 0x01,
			EN_RX_ADDR = 0x02,
			SETUP_AW = 0x03,
			SETUP_RETR = 0x04,
			RF_CH = 0x05,
			RF_SETUP = 0x06,
			STATUS = 0x07,
			OBSERVE_TX = 0x08,
			RCV_PWR_DETECT = 0x09,
			RX_ADDR_P0 = 0x0A,
			RX_ADDR_P1 = 0x0B,
			RX_ADDR_P2 = 0x0C,
			RX_ADDR_P3 = 0x0D,
			RX_ADDR_P4 = 0x0E,
			RX_ADDR_P5 = 0x0F,
			TX_ADDR = 0x10,
			RX_PW_P0 = 0x11,
			RX_PW_P1 = 0x12,
			RX_PW_P2 = 0x13,
			RX_PW_P3 = 0x14,
			RX_PW_P4 = 0x15,
			RX_PW_P5 = 0x16,
			FIFO_STATUS = 0x17,
			DYNPD = 0x1C,
			FEATURE = 0x1D,
			MAX
		};

		template<Type t_type, typename TBitField>
		struct Reg
		{
			static constexpr Type type = t_type;
			static constexpr uint8_t size = sizeof(TBitField);

			union
			{
				TBitField bf;
				uint8_t data[size];
			};

			inline Reg() {
				clear();
			}

			inline Reg(const TBitField &b):
				bf(b) {}

			template<typename... t_uint8_t>
			inline Reg(t_uint8_t... vals) {
				set(vals...);
			}

			inline Reg(const uint8_t (&d)[size]) {
				set(d);
			}

			inline void clear(void) {
				Mem::clr8(size, data);
			}

			inline void set(const TBitField &b) {
				bf = b;
			}

			inline void set(const uint8_t (&d)[size]) {
				Mem::cpy8(size, data, d);
			}

			template<typename... t_uint8_t>
			inline set(t_uint8_t... vals) {
				const uint8_t d[] { vals... };

				static_assert(sizeof(d) == size, "Wrong parameter count");

				set(d);
			}

			inline print(void) {
				Mem::print8(size, data);
			}
		};

		struct CfgBitField
		{
			uint8_t prim_rx     : 1;
			uint8_t pwr_up      : 1;
			uint8_t crc0        : 1;
			uint8_t en_crc      : 1;
			uint8_t mask_max_rt : 1;
			uint8_t mask_tx_ds  : 1;
			uint8_t mask_rx_dr  : 1;
			uint8_t res7        : 1;
		};

		struct EnChansBitField
		{
			uint8_t p0     : 1;
			uint8_t p1     : 1;
			uint8_t p2     : 1;
			uint8_t p3     : 1;
			uint8_t p4     : 1;
			uint8_t p5     : 1;
			uint8_t res6_7 : 2;
		};

		struct SetupAWBitField
		{
			uint8_t aw     : 2;
			uint8_t res2_7 : 6;
		};

		struct SetupRetrBitField
		{
			uint8_t arc : 4;
			uint8_t ard : 4;
		};

		struct RFChBitField
		{
			uint8_t ch   : 7;
			uint8_t res7 : 1;
		};

		struct RFSetupBitField
		{
			uint8_t res0       : 1;
			uint8_t rf_pwr     : 2;
			uint8_t rf_dr_high : 1;
			uint8_t pll_lock   : 1;
			uint8_t rf_dr_low  : 1;
			uint8_t res6       : 1;
			uint8_t cont_wave  : 1;
		};

		struct StatusBitField
		{
			uint8_t tx_full : 1;
			uint8_t rx_p_no : 3;
			uint8_t max_rt  : 1;
			uint8_t tx_ds   : 1;
			uint8_t rx_dr   : 1;
			uint8_t res7    : 1;
		};

		struct ObserveTxBitField
		{
			uint8_t arc_cnt  : 4;
			uint8_t plos_cnt : 4;
		};

		struct RPDBitField
		{
			uint8_t rpd    : 1;
			uint8_t res1_7 : 7;
		};

		struct AddrFullBitField
		{
			uint8_t addr[5];
		};

		struct AddrPartBitField
		{
			uint8_t addr[1];
		};

		struct RxPwPxBitField
		{
			uint8_t len    : 6;
			uint8_t res6_7 : 2;
		};

		struct FifoStatusBitField
		{
			uint8_t rx_empty : 1;
			uint8_t rx_full  : 1;
			uint8_t res2_3   : 2;
			uint8_t tx_empty : 1;
			uint8_t tx_full  : 1;
			uint8_t tx_reuse : 1;
			uint8_t res7     : 1;
		};

		struct FeatureBitField
		{
			uint8_t en_dyn_ack : 1;
			uint8_t en_ack_pay : 1;
			uint8_t en_dpl     : 1;
			uint8_t res3_7     : 5;
		};

		using Cfg        = Reg<CFG, CfgBitField>;
		using EnAA       = Reg<EN_AA, EnChansBitField>;
		using EnRxAddr   = Reg<EN_RX_ADDR, EnChansBitField>;
		using SetupAW    = Reg<SETUP_AW, SetupAWBitField>;
		using SetupRetr  = Reg<SETUP_RETR, SetupRetrBitField>;
		using RFCh       = Reg<RF_CH, RFChBitField>;
		using RFSetup    = Reg<RF_SETUP, RFSetupBitField>;
		using Status     = Reg<STATUS, StatusBitField>;
		using ObserveTx  = Reg<OBSERVE_TX, ObserveTxBitField>;
		using RPD        = Reg<RCV_PWR_DETECT, RPDBitField>;
		using RxAddrP0   = Reg<RX_ADDR_P0, AddrFullBitField>;
		using RxAddrP1   = Reg<RX_ADDR_P1, AddrFullBitField>;
		using RxAddrP2   = Reg<RX_ADDR_P2, AddrPartBitField>;
		using RxAddrP3   = Reg<RX_ADDR_P3, AddrPartBitField>;
		using RxAddrP4   = Reg<RX_ADDR_P4, AddrPartBitField>;
		using RxAddrP5   = Reg<RX_ADDR_P5, AddrPartBitField>;
		using TxAddr     = Reg<TX_ADDR, AddrFullBitField>;
		using RxPwP0     = Reg<RX_PW_P0, RxPwPxBitField>;
		using RxPwP1     = Reg<RX_PW_P1, RxPwPxBitField>;
		using RxPwP2     = Reg<RX_PW_P2, RxPwPxBitField>;
		using RxPwP3     = Reg<RX_PW_P3, RxPwPxBitField>;
		using RxPwP4     = Reg<RX_PW_P4, RxPwPxBitField>;
		using RxPwP5     = Reg<RX_PW_P5, RxPwPxBitField>;
		using FifoStatus = Reg<FIFO_STATUS, FifoStatusBitField>;
		using DynPD      = Reg<DYNPD, EnChansBitField>;
		using Feature    = Reg<FEATURE, FeatureBitField>;
	}

	packed_enum Cmd
	{
		R_REG = 0x00,
		W_REG = 0x20,
		R_PL = 0x61,
		W_PL = 0xA0,
		FLUSH_TX = 0xE1,
		FLUSH_RX = 0xE2,
		REUSE_TX_PL = 0xE3,
		R_RX_PL_WID = 0x60,
		W_ACK_PL = 0xA8,
		W_TX_PL_NOACK = 0xB0,
		NOP = 0xFF
	};

	struct Dev : SPIDev
	{
		static constexpr uint32_t getSPIClockHz(void) {
			return 8000000;
		}

		static constexpr uint8_t getSPIEndian(void) {
			return MSBFIRST;
		}

		static constexpr uint8_t getSPIMode(void) {
			return SPI_MODE0;
		}

		uint8_t cePin;
		IntMgr::ISRCtx isrCtx;

		inline Dev(uint8_t ss_pin, uint8_t ce_pin, int8_t int_pin = -1):
			SPIDev(ss_pin),
			cePin(ce_pin),
			isrCtx(int_pin, isrCb, this) {}

		static void isrCb(uint8_t pin, void *arg)
		{
			Dev &dev = reinterpret_cast<Dev &>(arg);

			// handle dyn pm
		}

		inline void boot(void)
		{
			pinMode(ssPin, OUTPUT);
			digitalWrite(ssPin, HIGH);

			pinMode(cePin, OUTPUT);
			digitalWrite(cePin, LOW);

			IntMgr::bindISR(isrCtx);
		}

		inline void beginTransation(void)
		{
			SPI.beginTransaction(SPISettings(getSPIClockHz(), getSPIEndian(), getSPIMode()));
			digitalWrite(ssPin, LOW);
		}

		inline uint8_t transfer(uint8_t b)
		{
			return SPI.transfer(b);
		}

		inline void transfer(void *data, size_t size)
		{
			SPI.transfer(data, size);
		}

		inline void endTransaction(void)
		{
			digitalWrite(ssPin, HIGH);
			SPI.endTransaction();
		}

		inline void regStatusRead(NRF24L01P::Reg::Status &status)
		{
			beginTransation();
			status.set(transfer(Cmd::NOP));
			endTransaction();
		}

		template<class R>
		inline void regRead(R &reg)
		{
			regRead(R::type, R::size, reg.data);
		}

		inline void regRead(Reg::Type type, uint8_t size, uint8_t data[])
		{
			beginTransation();

			transfer(Cmd::R_REG | (type & 0x1F));

			while (size--)
				*data++ = transfer(Cmd::NOP);

			endTransaction();
		}

		template<class R>
		inline void regWrite(const R &reg)
		{
			regWrite(R::type, R::size, reg.data);
		}

		inline void regWrite(Reg::Type type, uint8_t size, const uint8_t data[])
		{
			beginTransation();

			transfer(Cmd::W_REG | (type & 0x1F));

			while (size--)
				transfer(*data++);

			endTransaction();
		}

		template<typename T>
		inline void payloadWrite(const T &data)
		{
			const uint8_t *d = reinterpret_cast<const uint8_t *>(&data);

			payloadWrite(sizeof(data), d);
		}

		inline void payloadWrite(uint8_t size, const uint8_t data[])
		{
			beginTransation();

			transfer(Cmd::W_PL);

			while (size--)
				transfer(*data++);

			endTransaction();
		}

		template<typename T>
		inline void payloadRead(T &data)
		{
			uint8_t *d = reinterpret_cast<uint8_t *>(&data);

			payloadRead(sizeof(data), d);
		}

		inline void payloadRead(uint8_t size, uint8_t data[])
		{
			beginTransation();

			transfer(Cmd::R_PL);

			while (size--)
				*data++ = transfer(Cmd::NOP);

			endTransaction();
		}
	};

	/*
	 * The only way to fully reset the NRF is to disconnect it from
	 * the power supply, which is out of the scope of this library.
	 * This interface enables power management handling provided by
	 * the application.
	 */
	struct IExtPwrMgr
	{
		packed_enum Status
		{
			UNKNOWN = -1,
			OFF = 0,
			ON = 1
		};

		virtual Status getExtPwr(void) = 0;
		virtual int8_t setExtPwr(Status status) = 0;
	};

	struct RFDev : RFNet::IDevLayer, RFNet::IPipeLayer<uint8_t>, IExtPwrMgr
	{
		/*** RFDev ***/

		private:
		Dev &dev;

		public:
		RFDev(Dev &dev):
			dev(dev) {}

		/*
		 * To avoid false noise detection, we need to break the preamble
		 * bit pattern using at least two msb with the same value.
		 * So the two MSB of addr are ignored and set to 0.
		 */
		inline int8_t setAddrBase(const uint8_t (&addr)[5])
		{
			RFNet::HWMode hwm;

			hwm = hwmode();
			if (hwm != RFNet::HWMode::VLP
					&& hwm != RFNet::HWMode::IDLE)
				return -1; // Error invalid state

			const uint8_t a[5] = {
				addr[0] & 0x3F,
				addr[1],
				addr[2],
				addr[3],
				addr[4]
			};

			Reg::RxAddrP0 r0(a);
			Reg::RxAddrP1 r1(a);

			dev.regWrite(r0);
			dev.regWrite(r1);
			return 0;
		}

		/*** RFNet::IDevLayer ***/

		public:
		RFNet::HWStatus hwstatus(void)
		{
			RFNet::HWMode hwm;

			hwm = hwmode();
			if (hwm == RFNet::HWMode::OFF)
				return RFNet::HWStatus::UNK;

			// TODO: Check if the device is in bad state

			return RFNet::HWStatus::READY;
		}

		RFNet::HWMode hwmode(void)
		{
			int8_t result;
			IExtPwrMgr::Status pwr_status;
			RFNet::HWMode hwmode;
			NRF24L01P::Reg::Cfg cfg;

			pwr_status = getExtPwr();
			if (pwr_status != ON)
				return RFNet::HWMode::OFF;

			dev.regRead(cfg);

			if (!cfg.bf.pwr_up)
				return RFNet::HWMode::VLP;

			result = *portOutputRegister(digitalPinToPort(dev.cePin)) & digitalPinToBitMask(dev.cePin);
			if (!result)
				return RFNet::HWMode::IDLE;

			if (cfg.bf.prim_rx)
				return RFNet::HWMode::RECV;

			return RFNet::HWMode::SEND;
		}

		int8_t hwmode(RFNet::HWMode hwmode_)
		{
			int8_t result;
			RFNet::HWMode src;
			RFNet::HWMode tgt;
			NRF24L01P::Reg::Cfg cfg;

			src = hwmode();
			tgt = hwmode_;

			/*** No change ***/

			if (tgt == src)
				return 0;

			dev.regRead(cfg);

			/*** Special cases ***/

			if (tgt == RFNet::HWMode::OFF)
			{
				/* Config will be lost anyway, no need to be subtle */
				return setExtPwr(OFF);
			}

			if (tgt == RFNet::HWMode::RST)
			{
				result = setExtPwr(OFF);
				if (result == 0)
					result = hwmode(RFNet::HWMode::VLP);

				return result;
			}

			if ((src == RFNet::HWMode::RECV
					&& tgt == RFNet::HWMode::SEND)
				|| (src == RFNet::HWMode::SEND
					&& tgt == RFNet::HWMode::RECV))
			{
				/* Go to idle first */
				digitalWrite(dev.cePin, LOW);
				cfg.bf.prim_rx = tgt == RFNet::HWMode::RECV;
				dev.regWrite(cfg);

				src = RFNet::HWMode::IDLE;
			}

			/*** Power up ***/

			if (src < tgt)
			{
				if (src == RFNet::HWMode::OFF)
				{
					result = setExtPwr(ON);
					if (result < 0)
						return -1;

					// Power on reset
					delay(100);

					src = RFNet::HWMode::VLP;
				}

				if (src == tgt)
					return 0;

				if (src == RFNet::HWMode::VLP)
				{
					cfg.bf.pwr_up = 1;
					dev.regWrite(cfg);
					delay(5);

					src = RFNet::HWMode::IDLE;
				}

				if (src == tgt)
					return 0;

				if (src == RFNet::HWMode::IDLE
						&& (tgt == RFNet::HWMode::RECV || tgt == RFNet::HWMode::SEND))
				{
					cfg.bf.prim_rx = tgt == RFNet::HWMode::RECV;
					dev.regWrite(cfg);

					digitalWrite(dev.cePin, HIGH);
					delayMicroseconds(130);

					src = tgt;
				}

				if (src == tgt)
					return 0;

				return -1;
			}

			/*** Power down ***/

			if (src == RFNet::HWMode::RECV || src == RFNet::HWMode::SEND)
			{
				digitalWrite(dev.cePin, LOW);

				src = RFNet::HWMode::IDLE;
			}

			if (src == tgt)
				return 0;

			if (src == RFNet::HWMode::IDLE)
			{
				cfg.bf.pwr_up = 0;
				dev.regWrite(cfg);

				src = RFNet::HWMode::VLP;
			}

			/* Transition to OFF already handled */

			if (src == tgt)
				return 0;

			return -1;
		}

		int8_t mode(RFNet::Mode mode)
		{
			switch (mode)
			{
				case RFNet::Mode::PIPE:
					return 0;

				case RFNet::Mode::RAW:
				default:
					return -1;
			}
		}

		/*** RFNet::IPipeLayer ***/

		public:
		int8_t genPAddr(PAddr &addr)
		{
			addr = 0;
			return -1;
		}

		int8_t popen(const PAddr &addr)
		{
			return -1;
		}

		int8_t pclose(const PAddr &addr)
		{
			return -1;
		}

		int8_t psend(const PAddr &raddr, uint8_t size, const void *data)
		{
			return -1;
		}

		int8_t precv(PAddr &raddr, uint8_t &size, void *data)
		{
			return -1;
		}

		/*** IExtPwrMgr ***/

		IExtPwrMgr::Status getExtPwr(void)
		{
			return ON;
		}

		int8_t setExtPwr(IExtPwrMgr::Status status)
		{
			if (status == ON)
				return 0;
			return -1;
		}
	};
}

#endif /* LIBARDUINO__RADIO__NRF24L01P_H */
