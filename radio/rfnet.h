#ifndef LIBARDUINO__RADIO__RFNET_H
#define LIBARDUINO__RADIO__RFNET_H 1

namespace RFNet
{
	/***** Layer interfaces *****/

	packed_enum HWStatus
	{
		UNK,   // Unknown status
		READY, // The device is fully operational
		BAD    // The device must be reset
	};

	/*
	 * If supported by the device, RECV and SEND can be implemented
	 * as a combined recv/send mode (no state transition between them).
	 * RFNet will use *recv() in RECV mode only, *send() in SEND mode only.
	 * The device retains the config unless it is put in OFF or RST mode.
	 * The modes are sorted from lesser to higher power usage.
	 */
	packed_enum HWMode
	{
		OFF,  // Power off mode
		RST,  // Reset, goes into VLP or IDLE when done
		VLP,  // Very low power mode, config is kept
		IDLE, // Low power mode, can resume to RECV or SEND quickly
		RECV, // Active recv mode
		SEND  // Active send mode
	};

	packed_enum Mode
	{
		RAW,
		PIPE
	};

	/*
	 * Mandatory interface
	 * The device supports basic state management
	 */
	struct IDevLayer
	{
		virtual HWStatus hwstatus(void) = 0;
		virtual HWMode hwmode(void) = 0;
		virtual int8_t hwmode(HWMode hwmode) = 0;
		/* Returns -1 if <mode> is not supported */
		virtual int8_t mode(Mode mode) = 0;
	};

	/*
	 * The device supports raw mode operation
	 * Data is exchanged with any available remote
	 */
	struct IRawLayer
	{
		/* <size> is read (data buf size) and written (actual read size) */
		virtual int8_t recv(uint8_t &size, void *data) = 0;
		virtual int8_t send(uint8_t size, const void *data) = 0;
	};

	/*
	 * The device supports pipe mode operation
	 * A connection between two devices is identified by the same pipe
	 * addr on both ends. Data is exchanged through an opened pipe.
	 */
	template <typename TPAddr>
	struct IPipeLayer
	{
		using PAddr = TPAddr;

		/* Generates a non-currently opened pipe addr, or < 0 if no more available (use pclose() to release addrs) */
		virtual int8_t genPAddr(PAddr &addr) = 0;
		virtual int8_t popen(const PAddr &addr) = 0;
		virtual int8_t pclose(const PAddr &addr) = 0;
		virtual int8_t precv(PAddr &raddr, uint8_t &size, void *data) = 0;
		virtual int8_t psend(const PAddr &raddr, uint8_t size, const void *data) = 0;
	};

	/***** Compat implementations *****/

	struct CompatPipeLayerImplFromRawLayer : IRawLayer, IPipeLayer<uint8_t>
	{
		// IRawLayer methods must be provided by the underlying impl

		// TODO: IPipeLayer methods
	};

	/***** High level protocols *****/

	template <typename TAddr>
	struct IServer
	{
		using Addr = TAddr;

		/* Set the listen addr */
		int8_t listen(const Addr &addr);
		int8_t accept(Addr &addr);
		int8_t close(const Addr &addr);
		int8_t recv(Addr &addr, uint8_t &size, void *data);
		int8_t send(const Addr &addr, uint8_t size, const void *data);
	};

	template <typename TAddr>
	struct IClient
	{
		// TODO
	};

	TT_SAN_TYPE_DECL(san_PAddr, PAddr);

	// TODO: Dyn pm (auto-toggle between RECV and VLP/IDLE (on/off timed pattern)
	template <typename TImpl>
	struct PipeServer : IServer<typename san_PAddr<TImpl>::type>
	{
		private:
		using Addr = typename san_PAddr<TImpl>::type;

		static_assert(std::is_convertible<TImpl&, IDevLayer&>::value, "TImpl must implement IDevLayer");
		static_assert(std::is_convertible<TImpl&, IPipeLayer<Addr>&>::value, "TImpl must implement IPipeLayer");

		public:
		TImpl &impl;
		Addr listenAddr;

		PipeServer(TImpl &impl_):
			impl(impl_) {};

		int8_t boot(void)
		{
			impl.mode(PIPE);
			impl.hwmode(RST);
			impl.hwmode(VLP);
		}

		int8_t listen(const Addr &addr)
		{
			int8_t result;

			result = impl.hwmode(IDLE);
			if (result < 0)
				goto fail_idle;

			result = impl.popen(addr);
			if (result < 0)
				goto fail_popen;

			result = impl.hwmode(RECV);
			if (result < 0)
				goto fail_recv;

			listenAddr = addr;
			return 0;

		fail_recv:
			impl.pclose(addr);

		fail_popen:
		fail_idle:
			return result;
		}

		struct AcceptPacket
		{
			int8_t cookie;
			Addr addr;
		};

		int8_t accept(Addr &addr)
		{
			int8_t result;
			AcceptPacket data;
			uint8_t sz = sizeof(data);

			result = recv(listenAddr, sz, &data);
			if (result < 0)
				return result;

			if (sz != sizeof(data))
				return -1; // TODO: Error invalid accept packet

			// data.cookie unchanged
			impl.genPAddr(data.addr);

			result = impl.popen(data.addr);
			if (result < 0)
				return result;

			result = send(listenAddr, sz, &data);
			if (result < 0)
				return result;

			return 0;
		}

		int8_t close(const Addr &addr)
		{
			return impl.pclose(addr);
		}

		int8_t recv(Addr &addr, uint8_t &size, void *data)
		{
			return impl.precv(addr, size, data);
		}

		int8_t send(const Addr &addr, uint8_t size, const void *data)
		{
			return impl.psend(addr, size, data);
		}
	};

	template <typename TImpl>
	struct PipeClient : IClient<typename san_PAddr<TImpl>::type>
	{
		private:
		TT_SAN_TYPE_DECL(san_PAddr, PAddr);
		using PAddr = typename san_PAddr<TImpl>::type;

		static_assert(std::is_convertible<TImpl&, IDevLayer&>::value, "TImpl must implement IDevLayer");
		static_assert(std::is_convertible<TImpl&, IPipeLayer<PAddr>&>::value, "TImpl must implement IPipeLayer");

		TImpl &impl;

		public:
		PipeClient(TImpl &impl_):
			impl(impl_) {};
	};
}

#endif /* LIBARDUINO__RADIO__RFNET_H */
