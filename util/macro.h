#ifndef LIBARDUINO__UTIL__MACRO_H
#define LIBARDUINO__UTIL__MACRO_H 1

#define NOINLINE __attribute__((noinline))

#define packed_enum enum __attribute__((__packed__))
#define packed_struct struct __attribute__((__packed__))

#endif /* LIBARDUINO__UTIL__MACRO_H */
