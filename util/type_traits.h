#ifndef LIBARDUINO__UTIL__TYPE_TRAITS_H
#define LIBARDUINO__UTIL__TYPE_TRAITS_H 1

namespace std
{
	template< class T > struct remove_cv                   { typedef T type; };
	template< class T > struct remove_cv<const T>          { typedef T type; };
	template< class T > struct remove_cv<volatile T>       { typedef T type; };
	template< class T > struct remove_cv<const volatile T> { typedef T type; };

	template< class T > struct remove_const                { typedef T type; };
	template< class T > struct remove_const<const T>       { typedef T type; };

	template< class T > struct remove_volatile             { typedef T type; };
	template< class T > struct remove_volatile<volatile T> { typedef T type; };

	template<class T, T v>
	struct integral_constant {
		static constexpr T value = v;
		using value_type = T;
		using type = integral_constant; // using injected-class-name
		constexpr operator value_type() const noexcept { return value; }
		constexpr value_type operator()() const noexcept { return value; } // since c++14
	};

	using false_type = std::integral_constant<bool, false>;
	using true_type = std::integral_constant<bool, true>;

	template<class T, class U>
	struct is_same : std::false_type {};

	template<class T>
	struct is_same<T, T> : std::true_type {};

	template< class T >
	struct is_void : std::is_same<void, typename std::remove_cv<T>::type> {};

	namespace detail {

		template <class T>
		struct type_identity { using type = T; }; // or use std::type_identity (since C++20)

		template <class T>
		auto try_add_lvalue_reference(int) -> type_identity<T&>;
		template <class T>
		auto try_add_lvalue_reference(...) -> type_identity<T>;

		template <class T>
		auto try_add_rvalue_reference(int) -> type_identity<T&&>;
		template <class T>
		auto try_add_rvalue_reference(...) -> type_identity<T>;

	} // namespace detail

	template <class T>
	struct add_lvalue_reference : decltype(detail::try_add_lvalue_reference<T>(0)) {};

	template <class T>
	struct add_rvalue_reference : decltype(detail::try_add_rvalue_reference<T>(0)) {};

	template<class T>
	typename std::add_rvalue_reference<T>::type declval() noexcept;

	template<typename _Tp>
		struct is_union
		: public std::integral_constant<bool, __is_union(_Tp)>
	{};

	namespace detail {
		template <class T>
		std::integral_constant<bool, !std::is_union<T>::value> test(int T::*);

		template <class>
		std::false_type test(...);
	}

	template <class T>
		struct is_class : decltype(detail::test<T>(nullptr))
	{};

	namespace detail {

		template<class T>
		auto test_returnable(int) -> decltype(
			void(static_cast<T(*)()>(nullptr)), std::true_type{}
		);
		template<class>
		auto test_returnable(...) -> std::false_type;

		template<class From, class To>
		auto test_implicitly_convertible(int) -> decltype(
			void(std::declval<void(&)(To)>()(std::declval<From>())), std::true_type{}
		);
		template<class, class>
		auto test_implicitly_convertible(...) -> std::false_type;

	} // namespace detail

	template<class From, class To>
	struct is_convertible : std::integral_constant<bool,
		(decltype(detail::test_returnable<To>(0))::value &&
		decltype(detail::test_implicitly_convertible<From, To>(0))::value) ||
		(std::is_void<From>::value && std::is_void<To>::value)
	> {};
}

/* Returns true when TSub exists under TParent */
#define TT_HAS_TYPE_DECL(name, TSub) \
	template <typename TParent> \
	struct name { \
		private: \
		typedef char yes[1]; \
		typedef char no[2]; \
		template <typename U> \
		struct type_check; \
		template <typename _1> \
		static yes& chk(type_check<typename _1::TSub> *); \
		template <typename> \
		static no& chk(...); \
		\
		public: \
		static constexpr bool value = sizeof(chk<TParent>(nullptr)) == sizeof(yes); \
	}

	/* Returns TSub when TSub exists under TParent, else TDefault */
	#define TT_SAN_TYPE_DECL(name, TSub) \
		template <typename TParent, typename TDefault = int> \
		struct name { \
			private: \
			template <typename _1> \
			static typename _1::TSub chk(void *); \
			template <typename> \
			static TDefault chk(...); \
			\
			public: \
			using type = decltype(chk<TParent>(nullptr)); \
		}

#endif /* LIBARDUINO__UTIL__TYPE_TRAITS_H */
