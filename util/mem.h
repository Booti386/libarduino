#ifndef LIBARDUINO__UTIL__MEM_H
#define LIBARDUINO__UTIL__MEM_H 1

struct Mem
{
	static inline void clr8(uint8_t size, uint8_t d[]) {
		while (size--)
			*d++ = 0;
	}

	static inline void cpy8(uint8_t size, uint8_t d[], const uint8_t s[]) {
		while (size--)
			*d++ = *s++;
	}

	static inline print8(uint8_t size, const uint8_t d[]) {
		while (size--)
		{
			Serial.print(*d++, HEX);
			Serial.print(' ');
		}

		Serial.print('\n');
	}
};

#endif /* LIBARDUINO__UTIL__MEM_H */
